const Routers = require('./routers');
const express = require("express");
const app = express();

app.use(express.json());
app.use((req, res, next) => {
    console.log('req.body', req.body);
    next()
})
Routers(app);
app.listen(process.env.PORT || 3000);

